
 import java.util.Scanner;
 public class lab2_1 {
   

    static int[] original = {1, 2, 3, 4, 5};
    public static int[] deleteElementByIndex(int[] arr, int index){
        int[] newArray = new int[arr.length-1];
        for(int i=0;i<index;i++){
            newArray[i] = arr[i];
        }
        System.out.print("Original: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+" ");
        }
        for(int i=index+1;i<arr.length;i++){
            newArray[i-1] = arr[i];
        }
        System.out.println();
       System.out.print("Array after deleting element with index "+index+" : [ ");
        for (int i = 0; i < newArray.length; i++) {
            System.out.print(newArray[i]+" ");
        }
        System.out.print("]");
        original = newArray;
        return original;
    }
    public static int[] deleteElementByValue(int[] arr, int value){
        int[] newArray = new int[arr.length-1];
        int j = 0;
        System.out.print("Original: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+" ");
        }
        for(int m=0;m<arr.length;m++){
            if(arr[m]==value){
                break;
            }else{
                newArray[m] = arr[m];
                j++;
            }
        }
        
        for(int l=j+1;l<arr.length;l++){
            newArray[l-1] = arr[l];
        }
        System.out.println();
        System.out.print("Array after deleting element with value "+value+" : [ ");
        for (int i = 0; i < newArray.length; i++) {
            System.out.print(newArray[i]+" ");
        }
        System.out.print("]");
        original = newArray;
        return original;
    }

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        deleteElementByIndex(original, 2);
        System.out.println();
        deleteElementByValue(original, 4);
    }
}

    

