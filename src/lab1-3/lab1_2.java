public class lab1_2 {
    public static void duplicate() { 
        int arr[] = { 1, 0, 2, 3, 0, 4, 5, 0 };
        System.out.println("Ex.1");
        System.out.print("Input: ");
        for (int num : arr) {
            System.out.print(num + " ");
        }
        System.out.println();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                for (int j = arr.length; j > i + 2; j--) {
                    arr[j - 1] = arr[j - 2];
                   
                }
                arr[i + 1] = 0;
                i += 1;

            }
            
        }
        System.out.print("Output: ");
        for (int num : arr) {

            System.out.print(num + " ");
        }
        System.out.println();
        int ex2[] = { 1, 2, 3 };
        System.out.println("Ex.2");
        System.out.print("Input: ");
        for (int num : ex2) {
            System.out.print(num + " ");
        }
        System.out.println();
        for (int i = 0; i < ex2.length; i++) {
            if (ex2[i] == 0) {
                for (int j = ex2.length; j > i + 2; j--) {
                    ex2[j - 1] = ex2[j - 2];
                }
                ex2[i + 1] = 0;
                i += 1;
            }
        }
        System.out.print("Output: ");
        for (int num : ex2) {
            System.out.print(num + " ");
        }
    }

    
    public static void main(String[] args) {
        System.out.println("Lab 1.2");
        duplicate();
        System.out.println();
        System.out.println();

    }
}

