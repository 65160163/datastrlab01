import java.util.Arrays;
public class lab01 {
   

class Arraymanipulate {
    static int numbers[] = { 5, 8, 3, 2, 7 };
    static String name[] = { "Alice", "Bob", "Charlie", "David" };
    static Double value[] = { 0.0, 0.0, 0.0, 0.0 };
    static String reservenames[] = { "", "", "", "" };

    //static int nums1[] = { 1, 2, 3, 0, 0, 0 };
    //static int m = 3;
    //static int nums2[] = { 2, 5, 6 };
    //static int n = 3;

    public static void printnumbers() { // lab1.1
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
    }

    public static void printnames() {
        for (String i : name) {
            System.out.print(i + " ");
        }
    }

    public static void showvalue() {
        value[0] = 3.14;
        value[1] = 2.3;
        value[2] = 3.78;
        value[3] = 5.21;
    }

    public static void printsumofnumbers() {
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        System.out.print("Sum of numbers = " + sum);
    }

    public static void printsortnumbers() {
        Arrays.sort(numbers);
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
    }

    public static void findmaxofvalue() {
        Double maxv = 0.0;
        for (int i = 0; i < value.length; i++) {
            if (i == 0) {
                maxv = value[0];
            } else {
                if (value[i] > maxv) {
                    maxv = value[i];
                }
            }
        }
    }

    public static void showreversename() {
        System.out.println();
        System.out.println("ReserveNames");

        reservenames[0] = name[3];
        reservenames[1] = name[2];
        reservenames[2] = name[1];
        reservenames[3] = name[0];

        for (int i = 0; i < reservenames.length; i++) {
            System.out.print(reservenames[i] + " ");
        }
    }

    

    public static void main(String[] args) {
        System.out.println("Lab 1.1");
        printnumbers();
        System.out.println();
        printnames();
        System.out.println();
        printsumofnumbers();
        System.out.println();
        findmaxofvalue();
        System.out.println();
        showreversename();
        System.out.println();
        printsortnumbers();
        System.out.println();
        System.out.println();
        
    }

    
}
}